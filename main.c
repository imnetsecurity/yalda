#include <stdio.h>
#include <unistd.h>

void drawCircle(int r)
{

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_BLACK      "\e[0;30m"
#define ANSI_COLOR_WHITE   "\e[0;39m"

                    // Consider a rectangle of size N*N
    int N = 2*r+1;
    int x, y;    // Coordinates inside the rectangle
                // Draw a square of size N*N.
    for (int i = N/1.5; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            // Start from the left most corner point
            x = i-r;
            y = j-r;

            int l=x*x + y*y;
            int rr= r*r+1;
            // If this point is inside the circle, print it
            if (l <= rr )
                if(l + 100 <= rr )
                        if(x%7 == 0 && y % 7 == 0 )
                            printf(ANSI_COLOR_BLACK "0" );
                        else
                            printf(ANSI_COLOR_RED "%d", l %2 );
                else
            if(i == r*2)
                printf(ANSI_COLOR_WHITE "*");
               else printf(ANSI_COLOR_GREEN "*");
            else // If outside the circle, print space
                printf(ANSI_COLOR_WHITE "*");
            printf(" ");
        }
        printf("\n");
    }
}

// Driver Program to test above function

int  main()
{
    char name[200];
    printf(" Give me your name: ");
    scanf( "%s",&name );
    printf("                                       Happy Yalda to you %s", name);
    sleep(1);
    printf("\n            ** STAY HOME STAY -- SAFE -- STAY STRONG ** We learn more in crisis than in comfort.\n");
    sleep(1);
    drawCircle(30);
    return 0;
}
